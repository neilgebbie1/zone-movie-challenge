module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es6: true,
		jest: true
	},
	settings: {
		react: {
			createClass: 'createReactClass',
			pragma: 'React', 
			version: '16.5.2', 
		},
		propWrapperFunctions: ['forbidExtraProps']
	},
	parser: 'babel-eslint',
	extends: ['eslint:recommended', 'plugin:react/recommended'],
	parserOptions: {
		ecmaVersion: 6,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true
		}
	},
	rules: { 
		'comma-dangle': 0,
		'react/jsx-uses-vars': 1,
		'react/display-name': 1,
		'react/no-unused-prop-types': 1,
		'no-mixed-spaces-and-tabs': 'off',
		'no-unused-vars': 'warn',
		'no-console': 1,
		'no-unexpected-multiline': 'warn',
		semi: ['error', 'never'],
		'no-case-declarations': 0,
		'jest/valid-expect': 0,
		'jest/no-disabled-tests': 'warn',
		'jest/no-focused-tests': 'error',
		'jest/no-identical-title': 'error',
		'jest/prefer-to-have-length': 'warn',
		'jest/valid-expect': 'error'
	},
	plugins: ['react', 'jest']
}
