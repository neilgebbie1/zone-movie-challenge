import React from 'react'
import propTypes from 'prop-types'
import GenreBlock from './genreBlock'
import ImageBlock from './imageBlock'

const MovieGrid = ({ title, formattedGenres, imageProps, poster_path }) => {
	return (
		<div className="movieBlock">
			<div>
				<h2>{title}</h2>
				<GenreBlock {...formattedGenres} />
			</div>
			<ImageBlock {...imageProps} posterPath={poster_path} />
		</div>
	)
}

MovieGrid.propTypes = {
	title: propTypes.string,
	formattedGenres: propTypes.array
}

export default MovieGrid
