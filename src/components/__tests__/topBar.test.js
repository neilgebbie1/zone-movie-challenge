import React from 'react'
import { shallow } from 'enzyme'
import TopBar from '../topBar'
import toJson from 'enzyme-to-json'
import { StandardButton } from 'components'

describe('Testing topBar component', () => {
	it('renders as expected', () => {
		const wrapper = shallow(<TopBar sideBarOpen={true} />)
		expect(toJson(wrapper)).toMatchSnapshot()
	})
	it('StandardButton displays the right text', () => {
		const wrapper = shallow(<TopBar sideBarOpen={false} />)
		const sBtn = wrapper.find(StandardButton)
		const sBtnProps = sBtn.last().props().text
		expect(sBtnProps).toEqual('Show Filters')
	})
	it('triggers the getdata function', () => {
		const spy = jest.fn()
		const wrapper = shallow(<TopBar sideBarOpen={false} getData={spy} />)
		const sBtn = wrapper.find(StandardButton)
		sBtn
			.first()
			.props()
			.onClick()
		expect(spy).toBeCalledTimes(1)
	})
})
