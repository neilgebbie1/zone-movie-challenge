import React from 'react'
import { shallow } from 'enzyme'
import GenreCheckBoxes from '../genreCheckBoxes'
import toJson from 'enzyme-to-json'

describe('Testing GenreCheckBoxes component', () => {
	it('renders as expected', () => {
		const handle = jest.fn()
		const wrapper = shallow(
			<GenreCheckBoxes id={1} name="first" handleChange={handle} />
		)
		expect(toJson(wrapper)).toMatchSnapshot()
    })
    it('handleChange gets called on our input', () => {
        const handle2 = jest.fn()
		const wrapper = shallow(
			<GenreCheckBoxes id={150} name="first" handleChange={handle2} />
        )
        wrapper.find('input').props().onChange()
		expect(handle2).toHaveBeenCalledWith(150)
    })
})
