import React from 'react'
import { shallow } from 'enzyme'
import GenreBlock from '../genreBlock'
import toJson from 'enzyme-to-json'

describe('Testing GenreBlock component', () => {
	it('renders as expected', () => {
		const wrapper = shallow(
			<GenreBlock
				data={[
					{ 0: 'Action', 1: 'Comedy' },
					{ 0: 'Action', 1: 'Adventure', 2: 'Science Fiction' }
				]}
			/>
		)
		expect(toJson(wrapper)).toMatchSnapshot()
	})
})
