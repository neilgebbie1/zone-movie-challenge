import React from 'react'
import propTypes from 'prop-types'
import { GenreCheckBoxes } from './'
import Rheostat from 'rheostat'

const SideBar = ({
	sideBarOpen,
	genreList,
	handleChange,
	handleRange,
	rangeValue
}) => {
	const sliderRange = Array.from({ length: 20 }, (v, k) => k + 1)
	return (
		<div id="sidebar" className={sideBarOpen ? 'open' : 'closed'}>
			<div id="sliderWrap">
				<h4>Popularity Rating</h4>
				{/* Our range slider only has increments of 1, so lets just divide our output */}
				<span>{rangeValue[0]/2}</span>
				<Rheostat
					min={0}
					max={20}
					values={rangeValue}
					snapPoints={sliderRange}
					onChange={t => handleRange(t)}
				/>
				<span>{rangeValue[1]/2}</span>
			</div>
			<ul>
				{genreList.map(g => {
					return <GenreCheckBoxes key={g.id} {...g} handleChange={handleChange} />
				})}
			</ul>
		</div>
	)
}

SideBar.propTypes = {
	sideBarOpen: propTypes.bool,
	genreList: propTypes.array,
	handleChange: propTypes.func,
	handleRange: propTypes.func,
	rangeValue: propTypes.array
}

export default SideBar
