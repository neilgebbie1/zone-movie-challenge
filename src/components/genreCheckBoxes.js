import React from 'react'
import propTypes from 'prop-types'

const GenreCheckBoxes = ({ id, name, handleChange }) => {
	return (
		<li>
			<span>{name}</span>
			{/* We could replace the span with something fancy to 'fake' a checkbox, but in this instance its fine */}
			<input type="checkbox" name="vehicle1" value="Bike" onChange={() => handleChange(id)}/>
		</li>
	)
}

GenreCheckBoxes.propTypes = {
    id: propTypes.number,
    name: propTypes.string,
    handleChange: propTypes.func
}

export default GenreCheckBoxes
