import React from 'react'
import propTypes from 'prop-types'

const StandardButton = props => {
	return (
		<button {...props} className="standard">
			{props.text}
		</button>
	)
}

StandardButton.propTypes = {
	onClick: propTypes.func,
    text: propTypes.string,
    className: propTypes.string
}

export default StandardButton
