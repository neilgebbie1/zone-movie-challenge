import React from 'react'
import propTypes from 'prop-types'
import { determineImageWidth } from 'functions'

const ImageBlock = props => {
	const { posterSizes, baseUrl, posterPath } = props
	return (
		<img
			src={baseUrl + determineImageWidth(posterSizes) + posterPath}
			alt="something"
		/>
	)
}

ImageBlock.propTypes = {
	posterSizes: propTypes.array,
	baseUrl: propTypes.string,
	posterPath: propTypes.string
}

export default ImageBlock
