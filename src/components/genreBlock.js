import React from 'react'

const GenreBlock = data => {
	return (
		<div id="genreBlock">
			{/* There are multiple ways to loop through objects and arrays */}
			{Object.keys(data).map(d => {
				return <p key={d}>{data[d]}</p>
			})}
		</div>
	)
}

export default GenreBlock
