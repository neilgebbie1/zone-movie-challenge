import React from 'react'
import { StandardButton } from 'components'

const TopBar = ({ openSidebar, sideBarOpen, getData }) => {
	return (
		<header>
			<div id="inner">
				<StandardButton onClick={() => getData()} text="Load more" />
				<h1>Some Movies</h1>
				<StandardButton
					onClick={openSidebar}
					text={!sideBarOpen ? 'Show Filters' : 'Hide Filters'}
				/>
			</div>
		</header>
	)
}

export default TopBar
