import React from 'react'
import propTypes from 'prop-types'
import { determineMessage } from 'functions'
import { svg } from 'assets'

const LoadingMessage = ({ loadingState }) => {
	const { message, icon } = determineMessage(loadingState)
	return (
		<div id="loader">
			<div id="inner">
				<div id="contentWrap">
					<img src={svg[icon]} alt={`${message} icon`} height="65" width="68" />
					<p>{message}</p>
				</div>
			</div>
		</div>
	)
}

LoadingMessage.propTypes = {
	loadingState: propTypes.number
}

export default LoadingMessage
