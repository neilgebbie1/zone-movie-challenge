import React from 'react'
import { LoadingMessage } from './components'
import { MovieLayout } from './layouts'
import { apiCalls } from './functions'

/*
	This is our main 'Smart' component, responsible for getting our data. 
	We don't need redux cause the build is so simple and we can just hoist things up and needed.
	You might consider this 'repeating' myself by essentially calling three similar try catch functions.
	I could of chained the functions or even did some kind of Promise.All or something.
	This way we it's easier to handle errors and easier to test. 
	Sometimes engineers make abstractions for the sake of it and in this instance there's not much point.
*/

class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			loadingState: 0,
			movieList: [],
			moviePage: 1,
			genreList: [],
			baseUrl: '',
			posterSizes: []
		}
		this.getData = this.getData.bind(this)
		this.getGenres = this.getGenres.bind(this)
		this.timeout = 500 // This is just a way to fake the loader
	}

	componentDidMount() {
		setTimeout(() => this.getConfig(), this.timeout)
	}


	async getConfig() {
		try {
			const configuration = await apiCalls(1, 'configuration')
			const configWeNeed = {
				baseUrl: configuration.images.base_url,
				posterSizes: configuration.images.poster_sizes
			}
			this.setState({ ...configWeNeed, loadingState: 1 })
		} catch (e) {
			alert(e)
		} finally {
			setTimeout(() => this.getGenres(), this.timeout)
		}
	}

	async getGenres() {
		try {
			const genreList = await apiCalls(1, 'genre/movie/list')
			this.setState({ genreList: genreList.genres, loadingState: 2 })
		} catch (e) {
			alert(e)
		} finally {
			setTimeout(() => this.getData(1), this.timeout)
		}
	}

	async getData() {
		const { movieList, moviePage, genreList } = this.state
		const page = moviePage + 1
		try {
			const newMovieList = await apiCalls(
				moviePage,
				'movie/now_playing',
				genreList,
				movieList
			)
			this.setState({ movieList: newMovieList, moviePage: page, loadingState: 3 })
		} catch (e) {
			alert(e)
		}
	}

	render() {
		const { loadingState } = this.state
		return loadingState < 3 ? (
			<LoadingMessage {...this.state} />
		) : (
			<MovieLayout getData={this.getData} {...this.state} />
		)
	}
}

export default App
