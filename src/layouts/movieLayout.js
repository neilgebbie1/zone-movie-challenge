import React from 'react'
import propTypes from 'prop-types'
import { MovieGrid, SideBar, TopBar } from 'components'
import { withFilters } from 'functions'

/**
 *
 * 
 *
 * In this instance we haven't used the constructor or named functions
 * This is ok - but I can't used Enzyme to test because it doesn't like arrow functions
 * Perhaps there is another testing framework that is ok with this, but I am not aware of it.
 */

class MovieLayout extends React.Component {
	state = {
		sideBarOpen: false,
		filterIds: [],
		rangeValue: [6, 20]
	}

	openSidebar = () => this.setState({ sideBarOpen: !this.state.sideBarOpen })

	handleChange = id => {
		const oldArray = this.state.filterIds
		const index = oldArray.indexOf(id)
		if (index > -1) {
			oldArray.splice(index, 1)
			this.setState({ filterIds: oldArray })
		} else {
			this.setState({ filterIds: [...oldArray, id] })
		}
	}

	handleRange = range => this.setState({ rangeValue: range.values })

	render() {
		const { movieList, baseUrl, posterSizes } = this.props
		const { filterIds, rangeValue } = this.state
		const imageProps = {
			baseUrl,
			posterSizes
		}
		const moviesWithFilters = withFilters(movieList, filterIds, rangeValue)
		return (
			<>
				<TopBar {...this.state} {...this.props} openSidebar={this.openSidebar} />
				<SideBar {...this.state} {...this.props} handleChange={this.handleChange} handleRange={this.handleRange}/>
				<div id="gridWrap">
					{moviesWithFilters.length > 0 ? (
						moviesWithFilters.map(m => {
							return <MovieGrid key={m.id} {...m} imageProps={imageProps} />
						})
					) : (
						<h3>No Movies found...</h3>
					)}
				</div>
			</>
		)
	}
}

MovieLayout.propTypes = {
	getData: propTypes.func,
	movieList: propTypes.array
}

export default MovieLayout
