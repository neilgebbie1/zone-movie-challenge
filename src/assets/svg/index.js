import equalizer from './equalizer.svg'
import document from './document.svg'
import download from './download.svg'

export { equalizer, document, download }
