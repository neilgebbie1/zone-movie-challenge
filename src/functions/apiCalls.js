import { apiUrl } from 'config'
import getMovies from './getMovies'

/**
 *
 * A Generic way to call the TMB api
 * @param {number} pageCount
 * @param {string} endpoint
 * @param {array} genres
 * @param {array} currentArr
 * @returns {(object|function)}
 */
const apiCalls = (pageCount, endpoint, genres, currentArr) => {
	/*
		I prefer the readability of .then in this instance
		We can use plain XHR requests if we need to support old old browsers, or browsers that don't support service workers
		Some of the api calls dont use a page count, but lets not diverge even though its not needed
	*/
	return fetch(
		`${apiUrl}${endpoint}?api_key=079b6494293ae294c685874d7d4db92e&language=en-US&page=${pageCount}`
	)
		.then(response => {
			if (response.status !== 200) {
				alert('Looks like there was a problem. Status Code: ' + response.status)
				return
			}
			// convert our response
			return response.json().then(data => {
				// either filter or return our response
				return endpoint === 'movie/now_playing'
					? getMovies(data, currentArr, genres)
					: data
			})
		})
		.catch(err => {
			alert('Fetch Error :-S', err)
		})
}

export default apiCalls
