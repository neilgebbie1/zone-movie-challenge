import compare from './compare'
/**
 *
 * A way to return the correct image size
 * @param {array} sizes
 * @returns {string}
 */

const determineImageWidth = sizes => {
	// remove the sizes we dont need
	const sizesToWorkWith = sizes.slice(0, -1)
	// convert our sizes into numbers
	const sizesAsNumberArray = sizesToWorkWith.map(x => Number(x.replace('w', '')))
	// follow our grid guidelines
	const iW = window.innerWidth
	// some people do not like this style - I don't particular mind
	const gridWidth = iW < 1250 ? (iW < 900 ? (iW < 640 ? 1 : 3) : 4) : 5
	const toCompare = iW / gridWidth
	// find the values that are suitable
	const filter = sizesAsNumberArray.filter(compare(toCompare))
	// convert our number back into a string
	const reBuild = 'w' + filter[0]
	// return the string OR the largest size
	return reBuild || sizesToWorkWith[sizesToWorkWith.length]
}

export default determineImageWidth
