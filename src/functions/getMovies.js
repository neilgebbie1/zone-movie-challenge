import resultsWithGenres from './resultsWithGenres'

/**
 *
 * A way to add genres to our movies
 * @param {object} data
 * @param {array} currentArr
 * @param {array} genres
 * @returns {array}
 */
const getMovies = (data, currentArr, genres) => {
	const { results } = data
	const getResultsWithGenres = resultsWithGenres(results, genres)
	const newArr = [...currentArr, ...getResultsWithGenres] // or I could spread the function directly ...resultsWithGenres(results, genres)
	newArr.sort((a, b) => {
		return b.vote_average - a.vote_average
	})
	return newArr
}

export default getMovies
