/**
 *
 * A way to filter our genres based our selection
 * @param {array} dataToFilter
 * @param {array} filterIds
 * @returns {array}
 */
const checkGenres = (dataToFilter, filterIds) => {
	const newDataToFilter = dataToFilter.filter(d => {
		let finalResult = true
		filterIds.forEach(n => {
			// search through our filterid array for a match, ignore false
			if (d.genre_ids.includes(n) === false) finalResult = false
		})
		return finalResult
	})
	return newDataToFilter
}

export default checkGenres
