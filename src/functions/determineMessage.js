/**
 *
 * A way to return our loading message and icon
 * @param {number} loadingState
 * @returns {object}
 */
const determineMessage = loadingState => {
	switch (loadingState) {
		case 0:
			return {
				icon: 'equalizer',
				message: 'Loading Config...'
			}
		case 1:
			return {
				icon: 'document',
				message: 'Loading Genres...'
			}
		case 2:
			return {
				icon: 'download',
				message: 'Loading Movies...'
			}
		default:
			return {
				icon: 'equalizer',
				message: 'Loading...'
			}
	}
}

export default determineMessage
