/**
 *
 * A way to filter our movies based on a range
 * @param {array} dataToFilter
 * @param {array} range
 * @returns {array}
 */
const checkRange = (dataToFilter, range) => {
	const newDataToFilter = dataToFilter.filter(d => {
		// we need to double our vote average because our filter array doesn't do decimal places
		return d.vote_average * 2 > range[0] && d.vote_average * 2 < range[1]
	})
	return newDataToFilter
}

export default checkRange
