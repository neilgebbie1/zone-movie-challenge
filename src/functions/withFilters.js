import checkRange from './checkRange'
import checkGenres from './checkGenres'

/**
 *
 * A parent function to take the range and the check with the genre checkboxes
 * @param {array} data
 * @param {array} filterIds
 * @param {array} range
 * @returns {array}
 */

const withFilters = (data, filterIds, range) => {
	const withRange = checkRange(data, range)
	// return early, and dont filter if theres no filters
	if (filterIds.length < 1) return withRange
	const movies = checkGenres(withRange, filterIds)
	return movies
}

export default withFilters
