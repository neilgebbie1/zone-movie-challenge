/**
 *
 * A way to add genres to our movies
 * @param {array} results
 * @param {array} genres
 * @returns {array}
 */
const resultsWithGenres = (results, genres) => {
	// lets turn the genres into something like a hashmap
	const genreMap = genres.reduce((a, b) => {
		a[b.id] = b.name
		return a
	}, {})
	let formattedResults = []
	results.forEach(r => {
		const formattedGenres = r.genre_ids.map(genre => {
			return genreMap[genre]
		})
		r.formattedGenres = formattedGenres
		formattedResults.push(r)
	})
	return results
}

export default resultsWithGenres
