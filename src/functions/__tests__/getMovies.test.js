import getMovies from '../getMovies'

describe('getMovies Function', () => {
	const incomingData = {
		something: 'this',
		results: [
			{ vote_average: 1, genre_ids: [1, 2] },
			{ vote_average: 5, genre_ids: [2, 3] },
			{ vote_average: 10, genre_ids: [1] }
		]
	}
	const oldArray = [
		{ vote_average: 6, formattedGenres: ['one'] },
		{ vote_average: 7, formattedGenres: ['one', 'two'] },
		{ vote_average: 9, formattedGenres: ['one', 'two', 'three'] }
	]
	const genres = [
		{ id: 1, name: 'one' },
		{ id: 2, name: 'two' },
		{ id: 3, name: 'three' }
	]

	it('Formats the results into movies and genres', () => {
		const expected = [
			{ formattedGenres: ['one'], genre_ids: [1], vote_average: 10 },
			{ formattedGenres: ['two', 'three'], genre_ids: [2, 3], vote_average: 5 },
			{ formattedGenres: ['one', 'two'], genre_ids: [1, 2], vote_average: 1 }
		]
		const check = getMovies(incomingData, [], genres)
		expect(check).toEqual(expected)
	})
	it('Adds and formats the old array', () => {
		const expected = [
			{ formattedGenres: ['one'], genre_ids: [1], vote_average: 10 },
			{ formattedGenres: ['one', 'two', 'three'], vote_average: 9 },
			{ formattedGenres: ['one', 'two'], vote_average: 7 },
			{ formattedGenres: ['one'], vote_average: 6 },
			{ formattedGenres: ['two', 'three'], genre_ids: [2, 3], vote_average: 5 },
			{ formattedGenres: ['one', 'two'], genre_ids: [1, 2], vote_average: 1 }
		]
		const check = getMovies(incomingData, oldArray, genres)
		expect(check).toEqual(expected)
	})
})
