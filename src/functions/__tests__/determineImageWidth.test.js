import determineImageWidth from '../determineImageWidth'

describe('determineImageWidth Function', () => {
	const sizes = ['w100', 'w200', 'w300', 'w400', 'w500', 'original']

	it('Returns the second biggest image as a default', () => {
		const check = determineImageWidth(sizes)
		expect(check).toEqual('w300')
	})
	it('Returns the next size above the window innerwidth', () => {
		Object.defineProperty(window, 'innerWidth', {
			value: 90
		})
		const check = determineImageWidth(sizes)
		expect(check).toEqual(sizes[0])
	})
})
