import checkRange from '../checkRange'

describe('checkGenres Function', () => {
	const dataToFilter = [
		{ vote_average: 3 },
		{ vote_average: 9 },
		{ vote_average: 1 }
	]

	it('Returns all results because they are within the range', () => {
		const check = checkRange(dataToFilter, [0, 10])
		expect(check).toEqual(dataToFilter)
	})
	it('Returns the one result because only one is above 8', () => {
		const check = checkRange(dataToFilter, [8, 10])
		expect(check).toEqual([{ vote_average: 9 }])
	})
	it('Returns the one result because only one is below 2', () => {
		const check = checkRange(dataToFilter, [0, 2])
		expect(check).toEqual([{ vote_average: 1 }])
	})
})
