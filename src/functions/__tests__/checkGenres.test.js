import checkGenres from '../checkGenres'

describe('checkGenres Function', () => {
	const dataToFilter = [{ genre_ids: [1, 2, 3] }, { genre_ids: [1] }]
	it('Returns both results because they both have 1', () => {
		const check = checkGenres(dataToFilter, [1])
        expect(check).toEqual(dataToFilter)

    })
    it('Returns the first result because one has 2', () => {
		const check = checkGenres(dataToFilter, [2])
        expect(check).toEqual(dataToFilter.slice(0,1))
	})
})
