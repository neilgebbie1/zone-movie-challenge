/**
 *
 * A way to compare two values
 * @param {number} value
 * @returns {bool}
 */
const compare = value => {
	return element => {
		return element >= value
	}
}

export default compare
