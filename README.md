# Hey there

### Intro
Hey I'm Neil Gebbie and this is for the zone coding challenge. Because I write my code in quite small chunks I didn't put tons of comments, but depending on the situation I might do more. Everything I've done is on purpose so if you see inconsistencies between modules it's not a mistake... I basically wanted to show that there is mulitple ways to do multiple things. I didn't use Redux because it's overkill for a small project like this. But I've used it extensively in the past.

I'll break things up into a handful of sections:
- Design
- Components
- Functions
- Dependencies
- Tests
- Get Started
- Known bugs

### Design
I'm not a designer - you probably know that by now. I didn't use any CSS framework(bootstrap, foundation etc),but I'm comfortable working with any of those frameworks - it's probably overkill these days, but it should be used on a project-by-project basis I guess. I haven't used Sass on a commerical level for about a year because I mostly use CSS-in-JS. It is subjective what is appropriate and also relative to the project.

I've written CSS for >6 years, so this isn't something that I thought was super important for the context of this test.

I used a mixture of flexbox, floats, absolute, fixed etc. I can just write flex, but I wanted to mix it up to show that I don't rely on one or the other. I'm comfortable with any and every layout. See my previous work for advanced responsive CSS layouts.

### Components
I kept a shallow folder structure cause it's such a simple project, nothing special here. Most of my components are Stateless functional, with the 2 parent components providing the majority of our functionality. In a bigger project I would use a deeper folder structure, and if theres a bunch more routes I'd either hoist up the parent or use Predictable state management(Redux etc).

Everything is a project-by-project use case.

### Dependencies
I used a minimum of dependencies - but I included one just to show that I can/will work with imported dependencies. I chose the slider because it was built by Airbnb and their fairly trustworthy. Due to a few issues with NPM ecosystem I may write my own range slider in a commerical setting(dependant on business decisions).

### Functions
I try to write everything in tiny testable components, not repeat myself etc. Sometime I'll write quite verbose method and function names just so anyone else in the team knows that I'm doing, expecially if your using ES6 spreads and tiny code blocks. I've tried to transform the data into objects and arrays because I'm comfortable with both, but the direction I took is fairly array heavy. 

### Tests
It's probably not worth me writing 100% test coverage on this one, but I've written some basic passing tests which you'll find in the tests folders or with the .test extension. I'm not an expert at writing tests but for the majority of use cases I can test my components comfortably.

### Get Started
- Before anything: `yarn install`
- Start the project: `yarn start`
- Run tests: `yarn test --verbose`
- Run tests with Coverage report `yarn test --coverage`
- Make a production build**: `yarn build`

** Follow the on screen prompts to serve the project.

### Known bugs
- Sometimes the movie api returns images that are smaller then the others.
- Sometimes the api returns duplicate results

I could of fixed all of these, but I listed them just in case you want to include these in the git readme.
